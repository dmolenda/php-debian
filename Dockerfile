FROM debian:11-slim

RUN apt-get -y update && apt-get -y upgrade; \
apt-get -y install php vim && rm -rf /var/lib/apt/lists/*

CMD ["/bin/bash"]
